<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shop');

/** MySQL database username */
define('DB_USER', 'shop');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XYz.n*[ipvB&l.Nr|,#gyO{K9Cmlf>NT931j<$##V2{r$M&[M1^rIu!D#nX{:+9q');
define('SECURE_AUTH_KEY',  'ShKt2D-^?MY,]|&bw%NO] ,qV&DfGU<-m(R^ 9$7>U-b)],mx<3_si.L[%-CdQa=');
define('LOGGED_IN_KEY',    ';^yTA:]]Pr.8+<j7LatQKn,,!^*8g[!^wrcNKrwe1im:US-v^#5:]OI+fRuQ@?av');
define('NONCE_KEY',        'T(*m?h5*$?8gF2XF+9x8N0;$h4]>tT>Xzz2e^|B2:^L`u@IkpP!R-Qkj^,Bp(-v3');
define('AUTH_SALT',        'Yhqc=Z;^J/_$:A)wl[$q/h?+#hYHkEL)p$=~.5GM:c@NKFH()-~-VR,c^qBW@Pwe');
define('SECURE_AUTH_SALT', ',^[:ir|QB S?2=[j_dR5Di(XKSV0{%`o:w1s>-`+#{Q_cYPNAe@XuQgN+Rw7KdfQ');
define('LOGGED_IN_SALT',   '|{5N*X~R})*BHUc3O~@28I9jopB(=ap>36@EJM!:l7[d%Y<1RqOgB[kyM!>FR/vF');
define('NONCE_SALT',       '=1JU;?u5N,n%4q*225Xr{S-4_O,w*N%yrhP`A[x][uo$n!3.!.]x[XLp:2`veK~e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
